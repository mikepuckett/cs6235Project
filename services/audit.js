(function() {
  'use strict';

  const dbConnectionService = require('./dbConnection.js');
  const mongo = require('mongodb');
  const Promise = require('promise');

  var auditCollection;

  dbConnectionService
      .getConnection()
      .then(conn => {
        auditCollection = conn.collection('audit');
      });

  module.exports = {

    addNewAuditEntityForUser: function addNewAuditEntityForUser (userId) {
      return auditCollection
          .insertOne({ userId: userId, records: [] });
    },

    addNewAuditRecord: function addNewAuditRecord (userId, flaggedMessage) {
      let auditRecord = {
        text: flaggedMessage.text,
        userId: flaggedMessage.commenterId,
        user: flaggedMessage.commenterName,
        channelId: flaggedMessage.channelId,
        channel: flaggedMessage.channelName,
        usersPresent: flaggedMessage.usersPresent,
        ts: flaggedMessage.ts,
        time: new Date().getTime()
      };
      let searchKey = {
        userId: new mongo.ObjectId(userId)
      };
      let updateOperation = {
        $push: {
          records: {
            $each: [auditRecord],
            $sort: { time: -1 }
          }
        }
      };

      return auditCollection
          .findOneAndUpdate(searchKey, updateOperation)
    },

    getForUser: function getForUser (userId) {
      return auditCollection
          .findOne({ userId: new mongo.ObjectId(userId) });
    },

    approveAuditRecord: function approveAuditRecord(userId, flaggedMessage) {
      let searchKey = {
        userId: new mongo.ObjectId(userId),
        'records.ts': flaggedMessage.ts
      };
      let updateOperation = {
        $set: {
          'records.$.approved': true
        }
      };
      return auditCollection
          .findOneAndUpdate(searchKey, updateOperation);
    },

    denyAuditRecord: function denyAuditRecord(userId, flaggedMessage) {
      let searchKey = {
        userId: new mongo.ObjectId(userId),
        'records.ts': flaggedMessage.ts
      };
      let updateOperation = {
        $set: {
          'records.$.denied': true
        }
      };
      return auditCollection
          .findOneAndUpdate(searchKey, updateOperation);
    }
  };
})();