(function() {
  const dbConnectionService = require('./dbConnection.js');
  const mongo = require('mongodb');
  const Promise = require('promise');

  var blacklistCache = {};
  var blacklistCollection;

  function updateBlacklistCache() {
    blacklistCollection
        .find()
        .toArray()
        .then(results => {
          
          results.forEach(blacklist => {
            blacklistCache[blacklist.userId] = blacklist;
          });

        });
  }

  dbConnectionService
      .getConnection()
      .then(conn => {
        blacklistCollection = conn.collection('blacklist');
        updateBlacklistCache();
      });

  module.exports = {
    addNewBlacklistForUser: function addNewBlacklistForUser (userId) {
      return blacklistCollection
          .insertOne({ userId: userId, list: [] });
    },

    deleteBlacklist: function deleteBlacklist (id) {
      return blacklistCollection
          .deleteOne({ '_id': new mongo.ObjectId(id) });
    },

    getById: function getById (id) {
      return blacklistCollection
          .findOne(new mongo.ObjectId(id));
    },

    getForUser: function getForUser (userId) {
      return blacklistCollection
          .findOne({ userId: new mongo.ObjectId(userId) });
    },

    listAll: function listAllBlacklists () {
      return blacklistCollection
          .find()
          .toArray();
    },

    update: function updateBlacklist (id, blacklist) {
      return new Promise((resolve, reject) => {
        if (id !== blacklist._id) {
          reject('blacklist ID mismatch');
        }

        blacklistCollection
            .findOneAndUpdate(new mongo.ObjectId(id), { $set: { list: blacklist.list } } )
            .then(result => {
              resolve(result);
              updateBlacklistCache();
            })
            .catch(err => {
              reject(err);
            });
      });
    },

    validateAgainstBlacklist: function validateAgainstBlacklist (userId, text, callback) {
      blacklistCache[userId].list.forEach(phrase => {
        if (text && text.toUpperCase().indexOf(phrase.toUpperCase()) > -1) {
          callback();
          return;
        }
      });
    }
  }

})();