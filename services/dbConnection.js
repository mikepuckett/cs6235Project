(function() {

  var acceptConnection;
  var declineConnection;
  let connectionPromise = new Promise((resolve, reject) => {
    acceptConnection = resolve;
    declineConnection = reject;
  });

  module.exports = {

    setConnection: function setConnection(connection) {
      acceptConnection(connection);
    },

    getConnection: function getConnection() {
      return connectionPromise;
    }
  }
})();