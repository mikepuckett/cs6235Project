(function() {

  const https = require('https');
  const mongo = require('mongodb');
  const Promise = require('promise');

  const dbConnectionService = require('../services/dbConnection.js');
  const tsgBot = require('../bot/tsgBot.js');
  const auditService = require('./audit.js');
  const blacklistService = require('./blacklist.js');
  const slackWebClient = require('./slackWebClient.js');

  const User = require('../domain/User.js');

  const SLACK_CLIENT_ID = process.env.SLACK_CLIENT_ID;
  const SLACK_CLIENT_SECRET = process.env.SLACK_CLIENT_SECRET;

  var userCollection;

  function submitGetRequest(url) {
    return new Promise((resolve, reject) => {
      https.get(url, res => {
        res.setEncoding('utf8');
        res.on('data', data => {
          resolve(JSON.parse(data));
        })
        .on('error', err => {
          reject(err);
        });
      });
    });
  }

  function addNewUser(userInfo) {
    return new Promise((resolve, reject) => {

      slackWebClient
          .getSlackUserName(userInfo.accessToken, userInfo.slackUserId)
          .then(userName => {
            
            userInfo.userName = userName;

            userCollection
                .insertOne(userInfo)
                .then(result => {
                  var user = result.ops[0];
                  
                  tsgBot.connect(user);
                  blacklistService.addNewBlacklistForUser(user._id);
                  auditService.addNewAuditEntityForUser(user._id);

                  resolve(new User(user));
                })
                .catch(err => {
                  reject(err);
                });
          })
          .catch(err => {
            reject(err);
          });
    });
  }

  function logUserIn(userInfo) {
    return new Promise((resolve, reject) => {

      userCollection
          .findOne({ slackUserId: userInfo.slackUserId })
          .then(result => {
            if (!result) {
              addNewUser(userInfo)
                  .then(addedUser => {
                    resolve(addedUser);
                  })
                  .catch(err => {
                    reject(err);
                  });
            } else {
              resolve(new User(result));
            }
          })
          .catch(err => {
            reject(err);
          });
    });
  }

  dbConnectionService
      .getConnection()
      .then(conn => {
        userCollection = conn.collection('user');
      });

  module.exports = {

    submitSlackAuthRequest: function submitSlackAuthRequest(code) {
      return new Promise((resolve, reject) => {

        const queryParams = [
          'client_id=' + SLACK_CLIENT_ID,
          'client_secret=' + SLACK_CLIENT_SECRET,
          'code=' + code
        ];

        const url = 'https://slack.com/api/oauth.access?' + queryParams.join('&');

        submitGetRequest(url)
            .then(dataJson => {

              const slackUserId = dataJson.user_id;
              const accessToken = dataJson.access_token;
              const bot = dataJson.bot;

              logUserIn({
                slackUserId: slackUserId,
                accessToken: accessToken,
                bot: bot
              })
              .then(user => {
                resolve(user);
              })
              .catch(err => {
                reject(err);
              });
            })
            .catch(err => {
              reject(err);
            });
      });
    },

    getUserById: function(userId) {
      return new Promise((resolve, reject) => {
        userCollection
            .findOne({ '_id': new mongo.ObjectId(userId) })
            .then(result => { resolve(new User(result)); })
            .catch(err => { reject(err); })
      });
    }
  };

})();