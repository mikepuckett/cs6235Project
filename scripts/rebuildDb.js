'use strict';

const MongoClient = require('mongodb').MongoClient;
const Promise = require('promise');

let DATABASE_URL = process.env.MONGODB_URI || '';

let databaseCollections = [
  'audit',
  'blacklist',
  'user'
];

MongoClient.connect(DATABASE_URL, (err, db) => {
  if (err) {
    throw err;
  }

  console.log('Connected successfully to server');

  let dropCollectionsPromise = new Promise((resolve, reject) => {
    db.collections()
        .then(collections => {
          let dropPromises = [];

          collections.forEach(collection => {
            db.dropCollection(collection.collectionName);
          });

          Promise
              .all(dropPromises)
              .then(result => {
                console.log('Dropped collections');
                resolve(result);
              })
              .catch(err => {
                reject(err);
              });
        })
        .catch(err => {
          reject(err);
        });
  });

  let createCollectionsPromise = new Promise((resolve, reject) => {
    dropCollectionsPromise
        .then(result => {
          let createPromises = [];

          databaseCollections.forEach(collection => {
            createPromises.push(db.createCollection(collection));
          });

          Promise
              .all(createPromises)
              .then(result => {
                console.log('Added Collections');
                resolve(result);
              })
              .catch(err => {
                reject(err);
              });
        })
        .catch(err => {
          reject(err);
        });
  });

  let createIndicesPromise = new Promise((resolve, reject) => {
    createCollectionsPromise
        .then(result => {
          let createPromises = [];

          createPromises.push(db.createIndex('audit', 'userId'));
          createPromises.push(db.createIndex('blacklist', 'userId'));

          Promise 
              .all(createPromises)
              .then(result => {
                console.log('Added Indices');
                resolve(result);
              })
              .catch(err => {
                reject(err);
              });
        })
        .catch(err => {
          reject(err);
        });
  });

  let insertDataPromise = new Promise((resolve, reject) => {
    createIndicesPromise
        .then(result => {
          let insertPromises = [];

          Promise
              .all(insertPromises)
              .then(result => {
                console.log('Finished inserting data');
                resolve(result);
              })
              .catch(err => {
                reject(err);
              });
        })
        .catch(err => {
          reject(err);
        });
  });

  insertDataPromise
      .then(result => {
        console.log('Rebuild completed successfully');
        db.close();
      })
      .catch(err => {
        console.error(err);
        db.close();
      });
});