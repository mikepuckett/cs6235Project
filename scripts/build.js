'use strict';

var copy = require('copy');
var del = require('del');

const options = {
  flatten: false
};

const PUBLIC_FILE_PATH = '.public';

del([PUBLIC_FILE_PATH + '/**']).then(() => {
  copy('./webapp/**/**.html', PUBLIC_FILE_PATH, options, err => {
    if (err) {
      throw err;
    }
  });

  copy('./webapp/**/**.css', PUBLIC_FILE_PATH, options, err => {
    if (err) {
      throw err;
    }
  });

  let FOUNDATION_FILES = [
    './webapp/css/**.eot',
    './webapp/css/**.ttf',
    './webapp/css/**.woff',
    './webapp/css/svgs/**.svg',
  ];
  copy(FOUNDATION_FILES, PUBLIC_FILE_PATH + '/css', options, err => {
    if (err) {
      throw err;
    }
  });

  copy('./systemjs.config.js', PUBLIC_FILE_PATH, options, err => {
    if (err) {
      throw err;
    }
  });
});