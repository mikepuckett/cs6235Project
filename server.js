const bodyParser = require('body-parser');
const express = require('express');
const mongo = require('mongodb');
const morgan = require('morgan');

// require so that it will immediatedly begin to attempt
// to connect to slack teams
const bot = require('./bot/tsgBot.js');

const port = process.env.PORT || 8080;
const DATABASE_URL = process.env.MONGODB_URI || '';

const app = express();
app.use(bodyParser.json());
app.use(morgan('dev'));

// route rule for first part files required on the client (located in /public)
app.use(express.static(__dirname + '/.public'));
// route rule for third-party scripts required on the client (located in /node_modules)
app.use('/lib', express.static(__dirname + '/node_modules/'));
// expose /webapp for typescript source-maps
app.use('/webapp', express.static(__dirname + '/webapp/'));

const dbConnectionService = require('./services/dbConnection');

function prepareApp(dbConnection) {

  dbConnectionService.setConnection(dbConnection);
  
  const auditRouter = require('./api/auditRouter.js');
  app.use('/api/audit', auditRouter);

  const authenticationRouter = require('./api/authenticationRouter');
  app.use('/api/authentication', authenticationRouter);

  const blacklistRouter = require('./api/blacklistRouter.js')({
    express: express
  });
  app.use('/api/blacklist', blacklistRouter);

  // lastly, any other routes that have not been explicitly handles, we
  // send index.html so that the angular router can handle the route.
  app.get('/*', function(req, res) {
    res.sendfile('./webapp/index.html');
  });
}

mongo.MongoClient
    .connect(DATABASE_URL)
    .then(prepareApp)
    .catch(err => {
      throw err;
    });

app.listen(port, () => {
  console.log('Listening on port ' + port + '...');
});