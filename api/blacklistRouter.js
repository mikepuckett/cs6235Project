module.exports = function(dependencies) {

  const express = dependencies.express;
  const blacklistService = require('../services/blacklist.js');

  const router = express.Router();

  router.get('/', (req, res) => {
    blacklistService
        .getForUser(req.headers.userid)
        .then(blacklist => {
          res.status(200).send(blacklist);
        })
        .catch(err => {
          console.error(err);
          res.status(500).end();
        });
  });

  router.get('/:id', (req, res) => {
    blacklistService
        .getById(req.params.id)
        .then(result => {

          if (!result) {
            res.status(404).end();
          }

          res.send(result);
        })
        .catch(err => {
          console.error(err);
          res.status(404).end();
        });
  });

  router.put('/:id', (req, res) => {
    blacklistService
        .update(req.params.id, req.body)
        .then(result => {
          res.status(204).end();
        })
        .catch(err => {
          console.error(err);
          return res.status(500).end();
        });
  });

  router.delete('/:id', (req, res) => {
    blacklistService
        .deleteBlacklist(req.params.id)
        .then(result => {
          res.status(204).send();
        })
        .catch(err => {
          console.error(err);
          res.status(500).end();
        });
  });

  return router;
}