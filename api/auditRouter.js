(function () {
  'use strict';

  const express = require('express');

  const auditService = require('../services/audit.js');
  const tsgBot = require('../bot/tsgBot.js');

  const router = express.Router();

  router.get('/', (req, res) => {
    auditService
        .getForUser(req.headers.userid)
        .then(audit => {
          if (audit) {
            res.status(200).send(audit);
          } else {
            res.status(404).end();
          }
        })
        .catch(err => {
          console.error(err);
          res.status(500).end();
        });
  });

  router.post('/approve', (req, res) => {
    const userId = req.headers.userid;
    const flaggedMessage = req.body.message;
    tsgBot.approveMessage(userId, flaggedMessage);
    auditService
        .approveAuditRecord(userId, flaggedMessage)
        .then(result => {
          res.status(200).end();
        })
        .catch(err => {
          console.error(err);
          res.status(500).end();
        });
  });

  router.post('/deny', (req, res) => {
    const userId = req.headers.userid;
    const flaggedMessage = req.body.message;
    tsgBot.denyMessage(userId, flaggedMessage);
    auditService
        .denyAuditRecord(userId, flaggedMessage)
        .then(result => {
          res.status(200).end();
        })
        .catch(err => {
          console.error(err);
          res.status(500).end();
        });
  });  

  module.exports = router;
})();