(function() {
  'use strict';

  const CLIENT_EVENTS = require('@slack/client').CLIENT_EVENTS;
  const Promise = require('promise');
  const RtmClient = require('@slack/client').RtmClient;
  const RTM_EVENTS = require('@slack/client').RTM_EVENTS;
  const WebClient = require('@slack/client').WebClient;

  const dbConnectionService = require('../services/dbConnection.js');
  const blacklistService = require('../services/blacklist.js');
  const auditService = require('../services/audit.js');
  const slackWebClient = require('../services/slackWebClient.js');

  const FlaggedMessage = require('../domain/FlaggedMessage.js');

  const CHANNEL_NAMES = {};
  const USER_NAMES = {};
  const WEB_CLIENTS = {};
  const RTM_CLIENTS = {};

  // This flag can be used to turn off slack integration. This will be used
  // to turn off the integration from the heroku hosted instance while actively
  // developing locally, otherwise we would have two bot instances listening to
  // the same team simultaneously. 
  const IGNORE_SLACK = process.env.IGNORE_SLACK || false;

  const NOTIFICATION_MSG = 'posted a questionable message in ';
  const URL = (process.env.APP_URL || 'http://localhost:8080') + '/audit';

  function sendNotification(rtmClient, userId, channel, flaggedMessage) {
    let notificationTxt
        = `${flaggedMessage.commenterName} ${NOTIFICATION_MSG}${flaggedMessage.channelName}. Check ${URL} for more information.`;
    rtmClient.sendMessage(notificationTxt, channel);
  }

  function deleteMessage(webClient, flaggedMessage) {
    webClient.chat.delete(
      flaggedMessage.ts,
      flaggedMessage.channelId,
      { as_user: true }
    );
  }

  function replaceWithPlaceholder(webClient, rtmClient, flaggedMessage) {
    /*
     * The behavior of updating a flagged message with a placeholder must be mocked by deleting the original
     * message, and then immediately posting a new message in it's place. This is due to limitations in the Slack API
     * where a user may only update their own message, but an admin user can delete any user's post. So the admin
     * user deletes the questionable message through the Web API, and then the bot user "posts" the placeholder message
     * which means that the bot user can come back later and update it's own message after the admin has either approved
     * or denied the message.
     */
    deleteMessage(webClient, flaggedMessage);
    
    const placeholder = `[${flaggedMessage.commenterName}'s comment is being reviewed for appropriateness]`;

    return new Promise((resolve, reject) => {
      rtmClient.sendMessage(placeholder, flaggedMessage.channelId, (err, res) => {
        if (err) reject(err);
        // resolve the timestamp of the placeholder message so that it can be updated later if the admin approves
        // the original message
        resolve(res.ts);
      });
    });
  }

  function createAuditRecord(userId, flaggedMessage) {
    auditService.addNewAuditRecord(userId, flaggedMessage);
  }

  function connect(user) {

    const BOT_ACCESS_TOKEN = user.bot.bot_access_token;
    const BOT_USER_ID = user.bot.bot_user_id;
    const WEB_ACCESS_TOKEN = user.accessToken;
    const USER_ID = user._id;

    // the TRM client. Used for general bot interaction
    const rtm = new RtmClient(BOT_ACCESS_TOKEN, {logLevel: 'info'});
    // the web API client. Used for more sophisticated bot interaction
    const web = new WebClient(WEB_ACCESS_TOKEN);

    WEB_CLIENTS[USER_ID] = web;
    RTM_CLIENTS[USER_ID] = rtm;

    if (!(IGNORE_SLACK === 'false' || IGNORE_SLACK === false)) {
      console.info('Bypassing the Slack connection due to environment configuration');
      return;
    }
    rtm.start();

    let USER_CHANNEL_ID; // the channel ID for a DM to the authenticated user.
    rtm.on(CLIENT_EVENTS.RTM.AUTHENTICATED, rtmStartData => {
      console.info(`Logged in as ${rtmStartData.self.name} to team ${rtmStartData.team.name}`);

      rtmStartData.ims.forEach(im => {
        if (im.user === user.slackUserId) {
          USER_CHANNEL_ID = im.id;
        }
      });

    });

    // this event handler "listens" to every message that is sent in the Slack team
    rtm.on(RTM_EVENTS.MESSAGE, message => {

      if (message.channel === USER_CHANNEL_ID) {
        // ignore DMs with the authenticated user.
        return;
      }

      // validate the message against the team's blacklist'
      blacklistService.validateAgainstBlacklist(USER_ID, message.text, () => {
        /*
         * this callback function is called should the message fail the blacklist
         * validation. If the message does not fail validation, then this function
         * is never called.
         */
        const flaggedMessage = new FlaggedMessage(message);

        // map the (commenter's) userId to a userName, and the channelId to a channel name
        flaggedMessage.commenterName = USER_NAMES[USER_ID][flaggedMessage.commenterId];
        flaggedMessage.channelName = '#' + CHANNEL_NAMES[USER_ID][flaggedMessage.channelId];

        // make a web API request to get up-to-date information regarding the channel, so that
        // the list of users present can be set in the audit record
        const getUsersPromise = slackWebClient
            .getChannelById(WEB_ACCESS_TOKEN, flaggedMessage.channelId)
            .then(channel => {
              // map the userIds present in the channel to their user names
              flaggedMessage.usersPresent
                  = channel.members.map(memberId => USER_NAMES[USER_ID][memberId]);
            })
            .catch(err => { console.error(err); });
        
        // replace the flagged message with a placeholder until the comment has been reviewed.
        // This action is composed of two steps. First the flagged message is deleted through
        // the web API. Secondly, the bot user posts the placeholder message using the RTM API.
        const replaceWithPlaceholderPromise = replaceWithPlaceholder(web, rtm, flaggedMessage)
            .then(placeholderTs => {
              // replace the flagged messages timestamp with the timestamp of the placeholder
              // so that it can be updated after review
              flaggedMessage.ts = placeholderTs;
            });

        Promise.all([getUsersPromise, replaceWithPlaceholderPromise])
            .then(result => {
              // after the members present list and the placeholder timestamp have been resolved,
              // create the audit records so that an admin may review the message       
              createAuditRecord(USER_ID, flaggedMessage);
            });

        // notify the admin user through the RTM API that a questionable message has been
        // flagged.
        sendNotification(rtm, USER_ID, USER_CHANNEL_ID, flaggedMessage);
      })
    });

    // this event handler "listens" for new channels to be created
    rtm.on(RTM_EVENTS.CHANNEL_CREATED, newChannel => {
      slackWebClient.inviteUserToChannel(WEB_ACCESS_TOKEN, newChannel.channel.id, BOT_USER_ID);
      CHANNEL_NAMES[USER_ID][newChannel.channel.id] = newChannel.channel.name;
    });

    slackWebClient
        .getChannels(WEB_ACCESS_TOKEN)
        .then(channels => {

          CHANNEL_NAMES[USER_ID] = {};
          channels.forEach(channel => {
            slackWebClient.inviteUserToChannel(WEB_ACCESS_TOKEN, channel.id, BOT_USER_ID);
            CHANNEL_NAMES[USER_ID][channel.id] = channel.name;
          });

        });

    slackWebClient
        .getUsers(WEB_ACCESS_TOKEN)
        .then(users => {

          USER_NAMES[USER_ID] = {};
          users.forEach(user => {
            USER_NAMES[USER_ID][user.id] = user.name;
          });

        });
  }

  function initiateSlackConnections(dbConnection) {
    const userCollection = dbConnection.collection('user');

    userCollection
        .find()
        .toArray()
        .then(users => {
          users.forEach(connect);
        });
  }

  dbConnectionService
      .getConnection()
      .then(initiateSlackConnections);

  module.exports = {
    connect: connect,

    approveMessage: function approveMessage(userId, flaggedMessage) {
      const rtmClient = RTM_CLIENTS[userId];
      rtmClient.updateMessage({
        channel: flaggedMessage.channelId,
        ts: flaggedMessage.ts,
        text: `${flaggedMessage.user} said: ${flaggedMessage.text}`
      });
    },

    denyMessage: function denyMessage(userId, flaggedMessage) {
      const webClient = WEB_CLIENTS[userId];
      deleteMessage(webClient, flaggedMessage);
    }
  };
})();