import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';

import {AuthService} from '../auth/auth.service';
import {Blacklist} from './Blacklist';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class BlacklistService {

  private baseUrl = 'api/blacklist';
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(
    private http: Http,
    private authService: AuthService
  ) {}

  getAll(): Promise<any[]> {
    return this.http
        .get(this.baseUrl)
        .toPromise()
        .then(response => response.json() as any[])
        .catch(this.handleError);
  }

  getForCurrentUser(): Promise<Blacklist> {
    let userId = this.authService.getLocalUser().id;
    let headers = Object.assign({ userId: userId }, this.headers);

    return this.http
        .get(this.baseUrl, { headers: headers })
        .toPromise()
        .then(response => response.json() as Blacklist)
        .catch(this.handleError);
  }

  getById(id: string): Promise<any> {
    let idUrl = `${this.baseUrl}/${id}`;
    return this.http
        .get(idUrl)
        .toPromise()
        .then(response => response.json() as any)
        .catch(this.handleError);
  }

  update(blacklist: any): Promise<any> {
    const url = `${this.baseUrl}/${blacklist._id}`;
    return this.http
        .put(url, blacklist, {headers: this.headers})
        .toPromise()
        .then(() => blacklist)
        .catch(this.handleError);
  }

  add(blacklist: any): Promise<any> {
    return this.http
        .post(this.baseUrl, JSON.stringify(blacklist), {headers: this.headers})
        .toPromise()
        .then(res => res.json().data)
        .catch(this.handleError);
  }

  delete(id: string): Promise<void> {
    const url = `${this.baseUrl}/${id}`;
    return this.http
        .delete(url, {headers: this.headers})
        .toPromise()
        .then(() => null)
        .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
