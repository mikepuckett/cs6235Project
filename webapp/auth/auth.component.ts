import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute, Params, Router} from '@angular/router';

import {AuthService} from './auth.service';

@Component({
  moduleId: module.id,
  selector: 'slack-auth',
  template: ``,
  styleUrls: []
})

export class AuthComponent implements OnInit {
  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.route.queryParams.forEach((params: Params) => {
      const code = params['code'];

      this.authService
          .authenticate(code)
          .then(() => {
            this.router.navigateByUrl('/dashboard');
          });
    });
  }
}